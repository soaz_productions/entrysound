package main

import (
	"errors"
	"os"
	"time"
)

type entry struct {
	sound     [][]byte
	notBefore time.Time
}

// SoundMap holds sound data for users identified by User ID
type SoundMap struct {
	entries map[string]entry
}

// NewSoundMap creates a new SoundMap with no entries
func NewSoundMap() *SoundMap {
	return &SoundMap{
		entries: make(map[string]entry),
	}
}

// Get returns the sound data for the specified User ID
// and the earliest timestamp where next retrieval is allowed
func (s *SoundMap) Get(userID string) ([][]byte, time.Time, error) {
	e, ok := s.entries[userID]
	if !ok {
		return nil, time.Time{}, errors.New("No Sound for User")
	}
	if time.Now().UTC().After(e.notBefore) {
		newEntry := entry{sound: e.sound, notBefore: time.Now().UTC().Add(time.Second * 5)}
		s.entries[userID] = newEntry
	}
	if isAprilFools(time.Now()) {
		return s.GetRandomSound()
	}
	return get(e)

}

func get(e entry) ([][]byte, time.Time, error) {
	return e.sound, e.notBefore, nil
}

// Set adds the sound data for the specified User ID to the SoundMap
func (s *SoundMap) Set(userID string, sound [][]byte) {
	s.entries[userID] = entry{sound: sound, notBefore: time.Now().UTC()}
}

func isAprilFools(t time.Time) bool {
	_, force := os.LookupEnv("FORCE_APRIL_FOOLS")
	return (t.Month() == time.April && t.Day() == 1) || force
}
