package main

import (
	"flag"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/gomodule/redigo/redis"
	"github.com/heptiolabs/healthcheck"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type entrysoundJob struct {
	session          *discordgo.Session
	voiceStateUpdate *discordgo.VoiceStateUpdate
}

var logger logrus.FieldLogger
var userChannelMap = make(map[string]string)
var guildEventChannelMap = make(map[string]chan entrysoundJob)
var mut = &sync.Mutex{}
var soundMap = NewSoundMap()

var (
	token      = flag.String("token", "", "Token")
	updateAddr = flag.String("update", "", "The Address where to get Update Information")
	redisURL   = flag.String("redis", "", "Redis URL for Distributed Locking")
)

func main() {
	flag.Parse()
	validateFlags()
	discord, err := discordgo.New("Bot " + *token)
	logger = ProvideLogger()
	logger.Info("Starting up")
	if err != nil {
		panic(err)
	}
	soundLoader := ProvideSoundLoader()
	updateFunc := createUpdateFunc(soundLoader)
	err = updateFunc()
	if err != nil {
		panic(err)
	}
	pool := ProvideRedis()
	sigintChan := make(chan os.Signal, 1)
	signal.Notify(sigintChan, syscall.SIGINT)
	discord.AddHandler(voiceStateUpdateHandler)
	discord.AddHandler(text)
	health := createHealthHandler(pool)
	err = discord.Open()
	logger.Info("Starting watch")
	if err != nil {
		panic(err)
	}
	go http.ListenAndServe(":80", health)
	listenForUpdate(pool, logger.WithField("comp", "redis"), updateFunc)
	s := <-sigintChan
	logger.Infof("Exiting due to Signal %v", s)
	discord.Close()
}

func voiceStateUpdateHandler(discord *discordgo.Session, m *discordgo.VoiceStateUpdate) {
	voiceStateUpdateLogger := logger.WithField(
		"guild_id", m.GuildID)
	if _, ok := guildEventChannelMap[m.GuildID]; !ok {
		voiceStateUpdateLogger.Infof("Starting runner for Guild %s", m.GuildID)
		guildEventChannelMap[m.GuildID] = make(chan entrysoundJob, 5)
		go runSpeaker(guildEventChannelMap[m.GuildID])
	}
	voiceStateUpdateLogger.Info("Queuing VoiceStateUpdate for guild specific runner")
	guildEventChannelMap[m.GuildID] <- entrysoundJob{
		session:          discord,
		voiceStateUpdate: m,
	}
}

func runSpeaker(guildChan chan entrysoundJob) {
	for job := range guildChan {
		playerJoinLogger := logger.WithField(
			"user_id", job.voiceStateUpdate.UserID).WithField(
			"channel_id", job.voiceStateUpdate.ChannelID).WithField(
			"guild_id", job.voiceStateUpdate.GuildID)
		playerJoinLogger.Info("Processing Voice Status Update")
		oldChannel := userChannelMap[job.voiceStateUpdate.UserID]
		if oldChannel == job.voiceStateUpdate.ChannelID {
			playerJoinLogger.Info("User is still in the same channel, not playing")
			continue
		}
		userChannelMap[job.voiceStateUpdate.UserID] = job.voiceStateUpdate.ChannelID
		if len(job.voiceStateUpdate.ChannelID) == 0 {
			playerJoinLogger.Info("User left voice completely, not playing")
			continue
		}
		sound, notBefore, err := soundMap.Get(job.voiceStateUpdate.UserID)
		if err != nil {
			playerJoinLogger.WithError(err).Info("No Sound for User registered")
			continue
		}
		if !time.Now().UTC().After(notBefore) {
			playerJoinLogger.WithField("not_before", notBefore).Info("Entrysound is probably still playing, skipping")
			continue
		}
		err = playSound(job.session, job.voiceStateUpdate.GuildID, job.voiceStateUpdate.ChannelID, sound)
		if err != nil {
			playerJoinLogger.WithError(err).Error("Could not Play Sound")
		}
	}
}

func createUpdateFunc(soundLoader *SoundLoader) func() error {
	return func() error {
		conf, err := soundLoader.LoadConfig(*updateAddr)
		if err != nil {
			return errors.Wrap(err, "could not load config")
		}
		soundMap = NewSoundMap()
		err = soundLoader.LoadConfigSounds(conf, soundMap)
		if err != nil {
			return errors.Wrap(err, "Could not Load Sounds")
		}
		return nil
	}
}

func validateFlags() {
	if *updateAddr == "" {
		panic("Update Addr cannot be nothing")
	}
	if *token == "" {
		panic("Token should not be empty")
	}
}

func listenForUpdate(pool *redis.Pool, logger logrus.FieldLogger, updateFunc func() error) {
	conn := pool.Get()
	psc := redis.PubSubConn{Conn: conn}
	if err := psc.PSubscribe("update"); err != nil {
		conn.Close()
		logger.WithError(err).Panic("Could not Subscribe")
	}
	go func() {
		for {
			switch psc.Receive().(type) {
			case redis.Message:
				logger.Debug("Received Message on Update Channel")
				err := updateFunc()
				if err != nil {
					logger.WithError(err).Error("Could not update")
				}
			}
		}
	}()
}

func createHealthHandler(pool *redis.Pool) healthcheck.Handler {
	h := healthcheck.NewHandler()
	h.AddLivenessCheck("redis", func() error {
		c := pool.Get()
		_, err := c.Do("PING")
		return err
	})
	return h
}
