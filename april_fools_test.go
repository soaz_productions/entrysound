package main

import (
	"fmt"
	"testing"
	"time"
)

func TestIsAprilFools(t *testing.T) {
	fmt.Println(time.Now())
	fmt.Println(time.Now().Day())
	d := time.Date(2021, time.April, 1, 2, 22, 21, 0, time.UTC)
	if !isAprilFools(d) {
		t.Error("IsAprilFools(1. April) returned false")
	}
}
