package main

import (
	"encoding/binary"
	"io"
	"net/http"
	"os"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
)

func loadSound(data io.Reader) [][]byte {
	var opuslen int16
	var err error
	var buffer = make([][]byte, 0)
	for {
		// Read opus frame length from dca file.
		err = binary.Read(data, binary.LittleEndian, &opuslen)

		// If this is the end of the file, just return.
		if err == io.EOF || err == io.ErrUnexpectedEOF {
			logger.Info("Loaded Sound Data")
			return buffer
		}

		if err != nil {
			panic(err)
		}

		// Read encoded pcm from dca file.
		InBuf := make([]byte, opuslen)
		err = binary.Read(data, binary.LittleEndian, &InBuf)

		// Should not be any end of file errors
		if err != nil {
			panic(err)
		}

		// Append encoded pcm data to the buffer.
		buffer = append(buffer, InBuf)
	}
}

func loadSoundFromFile(filename string) ([][]byte, error) {
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer file.Close()
	return loadSound(file), nil
}

func loadSoundFromURL(url string) ([][]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, errors.Wrap(err, "Could not load sound from URL")
	}
	defer resp.Body.Close()
	return loadSound(resp.Body), nil
}

func playSound(discord *discordgo.Session, guildID, channelID string, sound [][]byte) error {
	vc, err := discord.ChannelVoiceJoin(guildID, channelID, false, true)
	defer vc.Close()
	defer vc.Disconnect()
	if err != nil {
		logger.WithError(err).Error("Could not join Voice Channel")
		return errors.Wrap(err, "Could not Join Voice Channel")
	}
	vc.Speaking(true)
	defer vc.Speaking(false)
	timeoutChan := time.NewTimer(time.Second * 10)
	for _, buff := range sound {
		select {
		case <-timeoutChan.C:
			return errors.New("Timeout sending over Opus")
		case vc.OpusSend <- buff:
			continue
		}
	}
	return nil
}
